import QtQuick 2.0

Item {
    id: root
    anchors.fill: parent
    width: parent.width
    height: parent.height

    signal lose(int score)

    property alias isrun: findplatform.running

    property int score: 0

    property bool leftpressed: false
    property bool rightpressed: false
    property bool jumpup: true
    property int timejump: 0
    property int timejumpmax: 35

    property int platfromheigth: 18
    property int platfromwidth: 60
    property int platformstartcounty: (root.height/3)/platfromheigth

    Item {
        id: item1
        anchors.fill: parent

        Image {
            id: image
            fillMode: Image.Tile
            anchors.fill: parent
            source: "resource/img/back.png"
            focus: true

            Repeater {
                id: platforms
                anchors.fill: parent
                model: platformsmodel
                delegate: Image {
                    id: platformelement
                    width: platfromwidth
                    height: platfromheigth
                    x: xx
                    y: yy
                    source: "resource/img/originalplatform.png"
                }
            }

            Image {
                id: doodle
                width: 64
                height: 64
                x:root.width/2-width/2
                y:root.height/2 -height/2
                fillMode: Image.Stretch
                sourceSize.height: 600
                sourceSize.width: 600
                source: "resource/img/doodle.png"
            }

        }
        Text {
            z:1
            height: 50
            text: "Score: " + score
            font.family: "Verdana"
            font.bold: true
            color: "#095104"
            verticalAlignment: Text.AlignLeft
            anchors.top: parent.top
            font.pixelSize: 20
        }

        ListModel { id: platformsmodel }

//        Keys.onLeftPressed: { leftpressed=true; }
//        Keys.onRightPressed: { rightpressed=true; }
//        Keys.onReleased: {
//            if (event.key === Qt.Key_Left) leftpressed=false;
//            else if (event.key === Qt.Key_Right) rightpressed=false;
//        }
//        Keys.onReturnPressed: start();


        Timer {
            id: findplatform;
            repeat: true;
            interval: 4
            onTriggered:  {
                doodle.y+=1;
                var needgenerate=true;
                var indexesdel = [];
                var l=0;
                for (var i=0; i < platforms.count; ++i) {
                    platforms.itemAt(i).y+=1;
                    if(needgenerate && platforms.itemAt(i).y < 0) needgenerate = false;
                    if(platforms.itemAt(i).y > root.height*2) {
                        indexesdel.push(platforms.itemAt(i).index);
                    }
                }
                for (i=0; i < indexesdel.length; ++i) {
                    platforms.model.remove(indexesdel[i]);
                }
                if(needgenerate) {
                    score += 50
                    var newcount=getRandomInt(15,25);
                    for(i=0; i<newcount; ++i)
                        platforms.model.append({"xx": getRandomInt(0, root.width - 20),
                                                   "yy": -getRandomInt(0, root.height- 20)});
                }

                if(leftpressed && (doodle.x-4 > 0)) doodle.x-=4;
                if(rightpressed && (doodle.x+68 < root.width)) doodle.x+=4;

                if(timejump < timejumpmax) {
                    ++timejump;
                    doodle.y = (jumpup) ? doodle.y-4 : doodle.y + 4;
                    if(doodle.y >= root.height-doodle.height+5) {
                        lose(score);
                    }
                    else if(doodle.y<0) doodle.y=0;
                }
                else {
                    jumpup = false;
                    timejump=0;
                }
                for (var i=0; i < platforms.count; ++i)
                {
                    var plx = platforms.itemAt(i).x;
                    var prx = platforms.itemAt(i).x + platforms.itemAt(i).width;
                    var pty = platforms.itemAt(i).y;
                    var pby = platforms.itemAt(i).y + platforms.itemAt(i).height;

                    var doodley = doodle.y+doodle.height;

                    if((doodle.y + doodle.height > pty)
                            && (doodle.y + doodle.height < pby)
                            && (doodle.x < prx)
                            && (doodle.x + doodle.width> plx)) {
                        jumpup = true;
                        timejump=0;
                        score += 2;
                        return;
                    }
                }
            }
        }
    }

    function getRandomInt(min, max) {
        var r = Math.random();
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function start()
    {
        score = 0;
        platforms.model.clear();
        doodle.y = root.height - platfromheigth*(platformstartcounty+2);
        var yi = 0;
        for(var xi=0; xi<(root.width+100)/platfromwidth; ++xi)
            for(yi=0; yi<platformstartcounty; ++yi) {
                platforms.model.append({"xx":xi*platfromwidth, "yy":root.height - yi*platfromheigth})
            }
        var newcount=getRandomInt(15,20);
        for(var i=0; i<newcount; ++i)
            platforms.model.append({"xx": getRandomInt(0, root.width - 20),
                                       "yy": getRandomInt(0, root.height- yi*platfromheigth)});

        if(findplatform.running) {
            findplatform.stop();
            platforms.model.clear();
            doodle.x = root.width/2-doodle.width/2
            doodle.y = root.height/2 -doodle.height/2
        }
        else findplatform.start();
    }
}
