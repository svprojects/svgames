import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.3

Rectangle {
    id: rectangle

    property int score: 0
    signal start()
    signal close()
    color: "#b3278a27"
    anchors.fill: parent

    Text {
        id: text1
        x: 171
        y: 128
        width: 213
        height: 84
        color: "#faf5c3"
        text: qsTr("Menu:")
        font.family: "Verdana"
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 43
    }

    Button {
        id: button
        x: 193
        y: 218
        width: 174
        height: 43
        text: qsTr("Start")
        font.bold: true
        font.pointSize: 18
        font.family: "Tahoma"
        wheelEnabled: false
        spacing: 6
        autoExclusive: false
        highlighted: true
        anchors.horizontalCenter: parent.horizontalCenter
        onClicked: start();
    }

    Button {
        id: button1
        x: 200
        y: 276
        width: 174
        height: 43
        text: qsTr("Exit")
        anchors.horizontalCenterOffset: 0
        font.family: "Tahoma"
        anchors.horizontalCenter: parent.horizontalCenter
        autoExclusive: false
        spacing: 6
        font.pointSize: 18
        font.bold: true
        wheelEnabled: false
        highlighted: true
        onClicked: close()
    }

    Text {
        id: text2
        x: 177
        y: 320
        width: 300
        height: 40
        color: "#faf5c3"
        text: "You score: " + score
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: "Verdana"
        font.pixelSize: 20
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.bold: true
    }
    states: [
        State {
            name: "normal"

            PropertyChanges {
                target: text3
                visible: true
            }

            PropertyChanges {
                target: text2
                visible: false
            }
        },
        State {
            name: "lose"

            PropertyChanges {
                target: text1
                text: qsTr("LOSE!")
            }

            PropertyChanges {
                target: button
                text: qsTr("Try again")
            }

            PropertyChanges {
                target: text2
                visible: true
            }
        }
    ]
    function lose(score) {
        rectangle.score = score;
        state = "lose";
    }
}
