import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    id: root
    visible: true
    width: 480
    height: 640
    title: qsTr("Doodle")


    Game {
        id: game
        onLose: {
            menu.lose(score)
            game.start()
        }
        Keys.onLeftPressed: { game.leftpressed=true; }
        Keys.onRightPressed: { game.rightpressed=true; }
        Keys.onReleased: {
            if (event.key === Qt.Key_Left) game.leftpressed=false;
            else if (event.key === Qt.Key_Right) game.rightpressed=false;
        }
        Keys.onReturnPressed: game.start();
    }
    Menu {
        id: menu
        visible: !(game.isrun)
        onStart: game.start();
        onClose: Qt.quit()
    }
}
