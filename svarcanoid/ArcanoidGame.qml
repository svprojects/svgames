import QtQuick 2.9
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4

Rectangle {
    id: root
    anchors.fill: parent
    width: parent.width
    height: parent.height
    color: "#290563"

    property bool leftpressed: false
    property bool rightpressed: false

    property int dx: -2
    property int dy: -2

    property int blockW: 25
    property int blockH: 10
    property int blockSpaceW: 2
    property int blockSpaceH: 2
    property double filling: 0.9
    property double fillingprize: 0.3
    property var colors: ["blue", "lightblue", "red", "pink", "violet", "green", "gray"]

    Item {
        id: mainItem
        anchors.rightMargin: 25
        anchors.leftMargin: 25
        anchors.topMargin: 30
        anchors.bottom: parent.verticalCenter
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottomMargin: 0

        Repeater {
            id: blocks
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            model: blocksmodel
            delegate: blocksdelegate
        }

        ListModel {
            id: blocksmodel
        }

        Component {
            id: blocksdelegate
            Rectangle {
                id: block
                x: xx;
                y: yy;
                color: clr
                width: blockW
                height: blockH
            }
        }
    }

    Rectangle {
        id: platform
        x: root.width/2-platform.width/2
        y: root.height-platform.height
        width: 30
        height: 7
        radius: 2
        color: "#ffffff"
    }

    Rectangle {
        id: ball
        x: root.width/2-ball.width/2
        y: root.height-platform.height-ball.height
        color: "orange"
        width: 8
        height: 8
        radius: 4
    }

    Timer {
        id: lifecicle;
        repeat: true;
        interval: 2
        onTriggered:  {

            if((ball.x+dx < 0) || (ball.x+dx > root.width)) dx = -dx;
            if((ball.y+dy < 0) || (ball.y+dy > root.height)) dy = -dy;

//            if(ifcross)

            ball.x+=dx;
            ball.y+=dy;

            if(leftpressed && (platform.x > 0)) platform.x-=4;
            if(rightpressed && (platform.x+platform.width < root.width)) platform.x+=4;


        }
    }


    //[x,y,width,heigth]
    function ifcross(rec1, rec2) {
        if(rec1.length < 4 || rec2.length < 4 ) return false;
         return ( (rec1.y < rec2.y+rec2.heigth)
                 || (rec1.y+heigth > rec2.y)
                 || (rec1.x+width < rec2.x)
                 || (rec1.x > rec2.x+width) );
    }

    function getRandomInt(min, max) {
        var r = Math.random();
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function generateField() {
        var field = [];
        var cCol = mainItem.width / (blockW+blockSpaceW);
        var cRow = mainItem.height / (blockH+blockSpaceH);

        for(var row=0; row<cRow; ++row){
            for(var col=0; col<cCol; ++col) {
                var r = Math.random();
                if(r < filling) {
                    var color = "white";
                    var index = Math.floor(r*10);
                    if(Math.random() < fillingprize) color = colors[Math.floor((r*10) % colors.length)]
                    blocks.model.append({"xx" : col*blockW+col*blockSpaceW - blockSpaceW,
                                            "yy" : row*blockH+row*blockSpaceH - blockSpaceH,
                                            "clr" : color});
                }
            }
        }

    }

    function start() {
        generateField();

        if(lifecicle.running) {
            lifecicle.stop();
            blocks.model.clear();
        }
        else lifecicle.start();
    }
}
