import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 480
    height: 640
    title: qsTr("Arcanoid")

    ArcanoidGame {
        id: game

        focus: true
        Keys.onReturnPressed: game.start();
        Keys.onLeftPressed: { game.leftpressed=true; }
        Keys.onRightPressed: { game.rightpressed=true; }
        Keys.onReleased: {
            if (event.key === Qt.Key_Left) game.leftpressed=false;
            else if (event.key === Qt.Key_Right) game.rightpressed=false;
        }

//        MouseArea {
//            anchors.fill: parent
//            onClicked: {
//                console.log("ok");
//                game.start();
//            }
//        }
    }
}
